<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

  <title>Yarn Berry</title>

  <link rel="stylesheet" href="css/reset.css" />
  <link rel="stylesheet" href="css/reveal.css" />
  <link rel="stylesheet" href="css/theme/white.css" />
  <link rel="stylesheet" href="css/custom.css" />

  <!-- Theme used for syntax highlighting of code -->
  <link rel="stylesheet" href="lib/css/monokai.css" />

  <!-- Printing and PDF exports -->
  <script>
    var link = document.createElement("link");
    link.rel = "stylesheet";
    link.type = "text/css";
    link.href = window.location.search.match(/print-pdf/gi)
      ? "css/print/pdf.css"
      : "css/print/paper.css";
    document.getElementsByTagName("head")[0].appendChild(link);
  </script>
</head>

<body>
  <div class="reveal">
    <div class="slides">
      <section>
        <h1>
          Yarn Berry
        </h1>
        <p><code>rm -rf ./**/node_modules</code></p>
      </section>
      <section>
        <h2>What is Yarn?</h2>
        <p>
          Yarn is an alternative package manager to npm. It's been around since 2016.
        </p>
        <p>
          Yarn's claim to fame is <em>stability</em>. It aims to have a deterministic installation; given a
          <code>yarn.lock</code> file, Yarn will always create the some <code>node_modules</code> tree.
        </p>
        <p>
          We use Yarn in the monorepo!
        </p>
      </section>
      <section>
        <h2>What is Yarn Berry?</h2>
        <p>
          Yarn Berry is the codename for a complete rewrite of the Yarn codebase that started two years ago.
        </p>
        <p>
          All new versions of Yarn (the latest is 3.1.0) live in the yarnpkg/berry GitHub repo.
        </p>
        <p>
          In addition to PnP-by-default, the new Yarn architecture is composed of small modules with intentional API
          surfaces. It also includes a plugin framework that can make use of these new modules.
        </p>
      </section>
      <section>
        <h2>Oh, also</h2>
        <p>You check it in to your repo!</p>
        <img src="images/yarn-berry-filetree.png">
        <aside class="notes">
          <p>This is actually really cool. This file rarely changes, it's not very large, and having it checked in means
            everyone is guaranteed to always be using the same version of the package manager</p>
        </aside>
      </section>
      <section>
        <h2>Why should I migrate off of Yarn v1?</h2>
        <p>
          New plugin API allows for really awesome new workflows, PnP improves performance and stability, and
          Zero-Installs improve development experience and CI performance.
        </p>
        <p>
          Also... <strong>Yarn v1 is deprecated</strong> and will not receive any future updates!
        </p>
      </section>
      <section>
        <section>
          <h2>What do I get out of it?</h2>
          <p>A bunch of stuff! But first we need to explain some things we've been glossing over.</p>
        </section>
        <section>
          <h3>Plug&apos;n&apos;Play</h3>
          <blockquote>
            It should be the package manager's job to inform the interpreter about the location of the packages on the
            disk and manage any dependencies between packages
          </blockquote>
          <aside class="notes">
            <p>
              Yarn already knows everything there is to know about your dependency tree - it even installs it on the
              disk for you. So, why is it up to Node to find where your packages are?
            </p>
            <ul>
              <li>
                Installs are now nearly instantaneous. Yarn only needs to generate a single text file (instead of
                potentially tens of thousands). The main bottleneck becomes the number of dependencies in a project
                rather than disk performance.
              </li>
              <li>
                Installs are more stable and reliable due to reduced I/O operations. Especially on Windows (where
                writing and removing files in batches may trigger various unintended interactions with Windows Defender
                and similar tools), I/O heavy node_modules operations were more prone to failure.
              </li>
              <li>
                Perfect optimization of the dependency tree (aka perfect hoisting) and predictable package
                instantiations.
              </li>
              <li>
                The generated .pnp.cjs file can be committed to your repository as part of the Zero-Installs effort,
                removing the need to run yarn install in the first place.
              </li>
              <li>
                Faster application startup! The Node resolution doesn't have to iterate over the filesystem hierarchy
                nearly as much as before (and soon won't have to do it at all!).
              </li>
            </ul>
          </aside>
        </section>
        <section>
          <h3>Zero-Installs</h3>
          <blockquote>
            Make your projects as stable and fast as possible by removing the main source of entropy from the equation:
            Yarn itself.
          </blockquote>
          <aside class="notes">
            <ul>
              <li>
                The cache folder is by default stored within your project folder (in .yarn/cache). Just make sure you
                add it to your repository (see also, Offline Cache).
              </li>
              <li>
                When running yarn install, Yarn will generate a .pnp.cjs file. Add it to your repository as well - it
                contains the dependency tree that Node will use to load your packages.
              </li>
            </ul>
          </aside>
        </section>
        <section>
          <h3>What does that actually get us, practically?</h3>
          <ol>
            <li>
              <em>Dramatically</em> reduced install time in CI. <code>git clone</code> takes an extra 15 or 20 seconds,
              and that&apos;s... it. Nothing else needs to be downloaded or written to disk. This led to a 4-minute+
              decrease in build time for Oak.
            </li>
            <li>
              Switching branches? That&apos;s all you need to do. No need to run <code>yarn install</code>,
              <code>yarn clean</code>, <code>rm -rf ./**/node_modules</code>, what have you.
            </li>
            <li>
              Yarn will now tell you if your packages attempt to import modules they don&apos;t declare a dependency on.
            </li>
            <li>
              Real plugins! Have a thing you wished Yarn did for you? <em>Now you can add support for that thing!</em>
            </li>
          </ol>
        </section>
      </section>
      <section>
        <h2>Ok, ok. But how?</h2>
        <ol>
          <li><code>.npmrc</code> &rightarrow; <code>.yarnrc.yml</code></li>
          <li>Janky <code>freeze-workspace</code> script &rightarrow; <code>yarn-plugin-isolate-workspace</code></li>
          <li>New <code>workspace:</code> protocol</li>
          <li>Install and set up Git LFS for <code>.cache/*.zip</code></li>
          <li>Install the Yarn SDKs for your editor</li>
          <li>
            Run <code>yarn install</code> and fix missing dependencies in your <code>packageExtensions</code>
            configuration. Repeat this ad nauseam until Yarn stops complaining.
          </li>
        </ol>
      </section>
    </div>
  </div>

  <script src="js/reveal.js"></script>

  <script>
    // More info about config & dependencies:
    // - https://github.com/hakimel/reveal.js#configuration
    // - https://github.com/hakimel/reveal.js#dependencies
    Reveal.initialize({
      controls: false,
      controlsTutorial: false,
      progress: false,
      hash: true,
      fragmentInUrl: true,
      preloadIframes: true,
      dependencies: [
        { src: "plugin/notes/notes.js", async: true },
        { src: "plugin/highlight/highlight.js", async: true }
      ]
    });
  </script>
  <!-- Fathom - simple website analytics - https://github.com/usefathom/fathom -->
  <script>
    (function (f, a, t, h, o, m) {
      a[h] =
        a[h] ||
        function () {
          (a[h].q = a[h].q || []).push(arguments);
        };
      (o = f.createElement("script")),
        (m = f.getElementsByTagName("script")[0]);
      o.async = 1;
      o.src = t;
      o.id = "fathom-script";
      m.parentNode.insertBefore(o, m);
    })(document, window, "//fathom.shanemoore.me/tracker.js", "fathom");
    fathom("set", "siteId", "AHXXA");
    fathom("trackPageview");
  </script>
  <!-- / Fathom -->
</body>

</html>