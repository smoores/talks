<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8" />
    <meta
      name="viewport"
      content="width-device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"
    />

    <title>Seeing Double</title>

    <link rel="stylesheet" href="css/reset.css" />
    <link rel="stylesheet" href="css/reveal.css" />
    <link rel="stylesheet" href="css/theme/white.css" />
    <link rel="stylesheet" href="css/custom.css" />

    <!-- Theme used for syntax highlighting of code -->
    <link rel="stylesheet" href="lib/css/monokai.css" />

    <!-- Printing and PDF exports -->
    <script>
      var link = document.createElement("link");
      link.rel = "stylesheet";
      link.type = "text/css";
      link.href = window.location.search.match(/print-pdf/gi)
        ? "css/print/pdf.css"
        : "css/print/paper.css";
      document.getElementsByTagName("head")[0].appendChild(link);
    </script>
  </head>

  <body>
    <div class="reveal">
      <div class="slides">
        <section>
          <h1>Seeing Double</h1>
          <blockquote>
            Ok, but hear me out, what if there were
            <strong>two</strong> ReactDOM renderers?
          </blockquote>
          <aside class="notes">
            <ul>
              <li>
                We're going to be talking today about some pretty advanced React
                topics, so buckle up.
              </li>
              <li>
                In particular, we're going to be talking about the distinction
                in React between the <em>reconciler</em> and the
                <em>renderers</em>.
              </li>
              <li>But first I'm going to give some context.</li>
            </ul>
          </aside>
        </section>
        <section>
          <section>
            <h2><em>~*~ record scratch ~*~</em></h2>
            <p>Why are we talking about this at all?</p>
          </section>
          <section>
            <h3>React &amp; ProseMirror: Best Frenemies</h3>
            <ul>
              <li>Both want control over the DOM</li>
              <li>
                So we give some DOM to ProseMirror, and some other DOM to React
              </li>
              <li>But now there&apos;s too much DOM!</li>
            </ul>
            <aside class="notes">
              <ul>
                <li>
                  Oak (collaborative rich text editor) uses two libraries for UI
                  rendering: React and ProseMirror.
                </li>
                <li>
                  You're probably all familiar with React. ProseMirror is a rich
                  text editing library for the browser. It's like if you gave
                  `contenteditable` a super-soldier serum.
                </li>
                <li>
                  Getting these two libraries to "play nice" has always been a
                  source of challenge in the Oak code base.
                </li>
              </ul>
            </aside>
          </section>
          <section>
            <h3>Here&apos;s our ProseMirror document:</h3>
            <pre><!--
           --><code class="typescript" data-trim>
                const doc = {
                  "type": "doc",
                  "content": [
                    {
                      "type": "paragraph",
                      "content": [
                        {
                          "type": "text",
                          "text": "Here's some text"
                        }
                      ]
                    }
                  ]
                }
              </code><!--
         --></pre>
          </section>
          <section>
            <h3>Here&apos;s our DOM:</h3>
            <pre><!--
           --><code class="html" data-trim>
                <!-- Here's our root View DOM -->
                <div class="ProseMirror" contenteditable="true">
                  <div>
                    <p> <!-- Here's our Paragraph node -->
                      <span>
                        Here's some text
                      </span>
                    </p>
                  </div>
                </div>
              </code><!--
         --></pre>
          </section>
          <section>
            <h3>What&apos;s going on with those extra elements?</h3>
            <pre><!--
           --><code class="html" data-trim>
                <!-- Here's our root View DOM -->
                <div class="ProseMirror" contenteditable="true">
                  <div> <!-- ProseMirror owns this -->
                    <p> <!-- React owns this -->
                      <span> <!-- ProseMirror owns this -->
                        Here's some text
                      </span>
                    </p>
                  </div>
                </div>
              </code><!--
         --></pre>
            <aside class="notes">
              <ul>
                <li>
                  React, as part of a very, very long-sighted strategy to
                  support new features like Suspense and Streams, renders
                  asynchronously. When you call `ReactDOM.render`, it might not
                  have actually produced any DOM by the time your next line of
                  code runs.
                </li>
                <li>
                  ProseMirror, on the other hand, wants you to synchronously
                  hand it the DOM for your NodeViews as soon is it asks for it.
                </li>
                <li>
                  This solution is like when you gave your younger siblings a
                  GameCube controller that wasn't actually hooked up to anything
                  so they would stop whining about when it would be their turn.
                  Or if you're me, this is what your younger siblings did to you
                  so that <em>you</em> would stop whining.
                </li>
                <li>
                  We're just giving ProseMirror "something to hold", but then we
                  turn around and tell React "ok, now that we got him to shut
                  up, you can go back to doing the important stuff. Just make
                  sure you put the result in that div there."
                </li>
              </ul>
            </aside>
          </section>
          <section>
            <h3>We just want this!</h3>
            <pre><!--
           --><code class="html" data-trim>
                <!-- Here's our root View DOM -->
                <div class="ProseMirror" contenteditable="true">
                  <p> <!-- Who owns this?? -->
                    Here's some text
                  </p>
                </div>
              </code><!--
         --></pre>
            <p>Why is this so hard?</p>
          </section>
        </section>
        <section>
          <h2>What would we need to do?</h2>
          <p class="fragment fade-in">
            The answer seems <em>maybe not so bad</em> at first glance.
          </p>
          <p class="fragment fade-in">
            We just need to ask React to render <em>first</em>, and then hand
            the result to ProseMirror.
          </p>
          <p class="fragment fade-in">Let&apos;s try it!</p>
        </section>
        <section>
          <section>
            <h3>Seems alright so far...</h3>
            <pre><code class="typescript" data-trim>
              const container = document.createElement('div');
              const root = createRoot(container);
              flushSync(() => {
                root.render(&lt;NodeViewWrapper { ... } /&gt;);
              });
            </code></pre>
          </section>
          <section>
            <h3>But if we zoom out...</h3>
            <pre><code class="typescript" data-trim="">
              useLayoutEffect(() => {
                ...
                const container = document.createElement('div');
                const root = createRoot(container);
                flushSync(() => {
                  root.render(&lt;NodeViewWrapper { ... } /&gt;);
                });
                ...
              });
            </code></pre>
            <code class="fragment fade-in error-message" data-trim="">
              Warning: flushSync was called from inside a lifecycle method.
              React cannot flush when React is already rendering. Consider
              moving this call to a scheduler task or micro task.
            </code>
            <p class="fragment fade-in">🤦</p>
            <aside class="notes">
              <ul>
                <li>
                  Because we're actually running this code within an effect
                  hook, this doesn't actually work. It's not possible to flush
                  the render <em>while React is already rendering.</em>
                </li>
                <li>
                  We can't do as the warning says, because we need React to be
                  rendered <em>synchronously</em>, and scheduler and micro tasks
                  are asynchronous by definition.
                </li>
                <li>But... maybe we can do something else?</li>
              </ul>
            </aside>
          </section>
        </section>
        <section>
          <h2>Recap</h2>
          <ul>
            <li class="fragment fade-in">
              We want to render React synchronously, so that we have something
              to hand our whiny little brother (ProseMirror).
            </li>
            <li class="fragment fade-in">
              We can&apos;t trigger a synchronous render of the React tree
              <em>from within a React render.</em>
            </li>
            <li class="fragment fade-in">
              ... We don&apos;t actually need to render the <em>whole</em> React
              tree.
            </li>
            <li class="fragment fade-in">
              We need a <em>secondary renderer.</em>
            </li>
          </ul>
          <aside class="notes">
            <ul>
              <li>
                We haven't really talked about this yet, but we don't need to
                render the whole React tree.
              </li>
              <li>
                We only need to render the single component that we want to hand
                to ProseMirror, like the Paragraph component.
              </li>
              <li>
                And, actually, that component isn't even really in the existing
                React tree yet; maybe we shouldn't really have to worry about
                whether we're already in a render cycle.
              </li>
              <li>What we need is a <em>secondary renderer.</em></li>
            </ul>
          </aside>
        </section>
        <section>
          <h2>Secondary what now?</h2>
          <ul>
            <li class="fragment fade-in">
              React is actually broken up into a few smaller parts.
            </li>
            <li class="fragment fade-in">
              There&apos;s a library, <code>react-reconciler</code>, that is
              responsible for producing the virtual document.
            </li>
            <li class="fragment fade-in">
              And then there are several "renderers", like React DOM and React
              Native, that know how to take changes indicated by the virtual
              document and turn them into "host" changes.
            </li>
          </ul>
        </section>
        <section>
          <h2>What does a renderer do?</h2>
          <p>
            Renderers implement methods like
            <code>createTextInstance()</code> and
            <code>appendChildToContainer()</code>.
          </p>
          <pre><code class="typescript" data-trim>
            createTextInstance(
              text,
              rootContainerInstance,
              hostContext,
              internalInstanceHandle,
            ) {
              return document.createTextNode(text);
            },
            
            appendChildToContainer(container, child) {
              container.appendChild(child);
            }
          </code></pre>
        </section>
        <section>
          <h2>Primary vs Secondary Renderers</h2>
          <ul>
            <li class="fragment fade-in">
              React DOM and React Native are "primary" renderers. There can be
              exactly one primary renderer per application.
            </li>
            <li class="fragment fade-in">
              There are also "secondary" renderers, like React ART, which is for
              rendering 2D graphics. There can be up to one secondary renderer
              per application.
            </li>
            <li class="fragment fade-in">
              React manages primary and secondary rendering contexts separately,
              which means that you can kick off a secondary render cycle from
              within a primary render cycle!
            </li>
          </ul>
        </section>
        <section>
          <pre
            style="width: initial"
          ><code class="typescript" data-trim style="max-height: 100%">
            import { createContainer, updateContainer } from 'react-reconciler';
              
            class Surface extends React.Component {
              componentDidMount() {
                const {height, width} = this.props;
                this._surface = Mode.Surface(+width, +height, this._tagRef);
                this._mountNode = createContainer(
                  this._surface,
                  LegacyRoot,
                  null,
                  false,
                  false,
                  '',
                );
                updateContainer(
                  this.props.children,
                  this._mountNode,
                  this
                );
              }
            ...
          </code></pre>
          <aside class="notes">
            <ul>
              <li>This is actual real code from ReactART</li>
              <li>
                Notice how this is calling React reconciler lifecycle methods
                from within a React lifecycle hook, the very thing we couldn't
                do before!
              </li>
            </ul>
          </aside>
        </section>
        <section>
          <h2>Can we do this with React DOM?</h2>
          <p class="fragment fade-in">
            Turns out... yes??
          </p>
          <pre class="fragment fade-in"><code class="typescript" data-trim>
            // export const isPrimaryRenderer = true;
            export const isPrimaryRenderer = false;  
          </code></pre>
          <pre class="fragment fade-in"><code class="shell" data-trim="">
            $ yarn build
            .../
          </code></pre>
          <pre class="fragment fade-in"><code class="shell" data-trim="">
            $ cp ./build/react-dom/react-dom.development.js \
              ../react-prosemirror/react-dom-secondary.development.js
          </code></pre>
          <aside class="notes">
            <ul>
              <li>
                So first I downloaded the React codebase and found the place
                where React DOM was declaring itself as a primary renderer
              </li>
              <li>Then I just... changed that to false</li>
              <li>
                Then I ran yarn build, which took <em>forever</em> (like
                literally 25 minues)
              </li>
              <li>
                And then I just took the resulting build output and threw in it
                the react-prosemirror repo to see what would happen.
              </li>
            </ul>
          </aside>
        </section>
        <section>
          <h2>Let&apos;s try this again...</h2>
          <pre><code class="typescript" data-trim="">
            // import { createRoot } from 'react-dom/client';
            import { createRoot } from 'react-dom-secondary/client';
              
            useLayoutEffect(() => {
              ...
              const container = document.createElement('div');
              const root = createRoot(container);
              flushSync(() => {
                root.render(&lt;NodeViewWrapper { ... } /&gt;);
              });
              ...
            });
          </code></pre>
          <p class="fragment fade-in">🎉</p>
          <aside class="notes">
            <ul>
              <li>
                This technically works! We can successfully render our React
                sub-tree synchronously, and then take the result and hand it off
                to ProseMirror. React and ProseMirror are actually playing the
                game together!
              </li>
              <li>
                It's not perfect, though. There are tradeoffs that we have to
                account for, now that we're using this secondary renderer.
              </li>
            </ul>
          </aside>
        </section>
        <section>
          <h2>Downsides</h2>
          <ul>
            <li class="fragment fade-in">
              Multiple React trees &mdash; rather than a single React tree that
              parents all elements, including node views, we now have a separate
              root for each node view.
            </li>
            <li class="fragment fade-in">
              No context sharing &mdash; React context doesn&apos;t
              automatically flow from the primary renderer to the secondary
              renderer.
              <ul>
                <li>
                  This can be manually mitigated, but it&apos;s not pretty!
                </li>
              </ul>
            </li>
          </ul>
        </section>
      </div>
    </div>
    <script src="js/reveal.js"></script>
    <script>
      Reveal.initialize({
        controls: false,
        controlsTutorial: false,
        progress: false,
        hash: true,
        fragmentInUrl: true,
        preloadIframes: true,
        dependencies: [
          { src: "plugin/notes/notes.js", async: true },
          { src: "plugin/highlight/highlight.js", async: true }
        ]
      });
    </script>
  </body>
</html>
