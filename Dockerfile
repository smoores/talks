FROM node:14
WORKDIR /usr/src/app

COPY package.json package-lock.json ./

RUN npm ci

COPY . .

RUN npm run build

CMD node ./node_modules/.bin/serve -l 8001
